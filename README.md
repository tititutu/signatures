# Suivi des signatures d'une pétition

Ce projet génère des courbes du nombre de signatures d'une pétition `SpeakOut! Email Petitions` (plugin wordpress) selon le temps.

## Installation

```bash
# créer un virtualenv python3 (par exemple dans ~/bin)
python3 -m venv ~/bin/signatures_venv
# sourcer ce nouveau virtualenv
source ~/bin/signatures_venv/bin/activate
# installer les dépendances python
(signatures_venv) pip install -r requirements.txt
```

## Génération des courbes

```bash
# instancier les variables selon votre environnment
(signatures_venv) DB_HOST=<DB_HOST> DB_USER=<DB_USER> DB_PASSWORD=<DB_PASSWORD> DB_NAME=<DB_NAME> python gen_graph.py
```

* `confirmed_signatures.png`: Nombre de signatures confirmées dans le temps
* `all_signatures.png`: Nombre de signatures (confirmées et non confirmées) dans le temps
